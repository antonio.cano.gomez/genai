import os
import fitz
import nltk
from nltk.tokenize import sent_tokenize
from openai import AzureOpenAI
from sentence_transformers import SentenceTransformer, util

nltk.download('punkt')

model = SentenceTransformer('paraphrase-MiniLM-L6-v2')

class RAGPrototype:
    def __init__(self, pdf_paths):
        self.pdf_paths = pdf_paths
        self.all_sentences = []
        self.all_embeddings = []
        self._preprocess()

    def _extract_text_from_pdf(self, pdf_path):
        doc = fitz.open(pdf_path)
        text = ""
        for page in doc:
            text += page.get_text()
        return text

    def _split_text_into_sentences(self, text):
        return sent_tokenize(text)

    def _embed_sentences(self, sentences):
        return model.encode(sentences)

    def _preprocess(self):
        for pdf_path in self.pdf_paths:
            text = self._extract_text_from_pdf(pdf_path)
            sentences = self._split_text_into_sentences(text)
            embeddings = self._embed_sentences(sentences)
            self.all_sentences.extend(sentences)
            self.all_embeddings.extend(embeddings)

    def _find_most_relevant_sentence(self, question_embedding):
        max_cos_sim = -1
        most_relevant_sentence = ""
        for i, sentence_embedding in enumerate(self.all_embeddings):
            cos_sim = util.pytorch_cos_sim(question_embedding, sentence_embedding)
            if cos_sim > max_cos_sim:
                max_cos_sim = cos_sim
                most_relevant_sentence = self.all_sentences[i]
        return most_relevant_sentence

    def _generate_text(self, sentence):
        client = AzureOpenAI(
            azure_endpoint = "https://confmind-oai3.openai.azure.com/",
            api_key=os.getenv("AZURE_OPENAI_KEY"),
            api_version="2024-02-15-preview"
        )

        message_text = [{"role":"system","content":sentence}]

        completion = client.chat.completions.create(
            model="RAGProto", # model = "deployment_name"
            messages = message_text,
            temperature=0.7,
            max_tokens=800,
            top_p=0.95,
            frequency_penalty=0,
            presence_penalty=0,
            stop=None
        )

        return completion.choices[0].message.content



    def answer_question(self, question):
        question_embedding = model.encode([question])
        most_relevant_sentence = self._find_most_relevant_sentence(question_embedding)
        return self._generate_text(most_relevant_sentence)

pdf_paths = ["corpus1.pdf", "corpus2.pdf", "corpus3.pdf"]
rag_prototype = RAGPrototype(pdf_paths)

question = "What is Europe?"
answer = rag_prototype.answer_question(question)
print("Answer:", answer)
